" -----------------------------------------------------------------------------------------
"           basic pref
" ------------------------------------------------------------------------------------------
" autocmd Clear
autocmd!

" ------------------------------------------------------------------------------------------
"       Define variables
" ------------------------------------------------------------------------------------------
" 使用OSを指定
let g:os = ""
if has('win32') || has('win64')
   let g:os = 'win'
elseif has('macunix')
   let g:os = 'macunix'
elseif has('unix')
   let g:os = "unix"
else
   let g:os = 'unknown'
endif

" .vimrcの場所により呼び出すfileの場所を設定
let $DotVim    = expand(fnamemodify($MYVIMRC, ':h') . '/.vim/')
let s:vimrcDir = expand($DotVim . "/vimrcs/")

" mapleaderを;に設定
let mapleader  = ";"


" ------------------------------------------------------------------------------------------
"           source vimrcs
" ------------------------------------------------------------------------------------------

" ファンクション, 変数を定義しているcommon file
execute "source " . s:vimrcDir . ".vimrc.common"
" スワップ、バックアップ,ファイル名などの設定
execute "source " . s:vimrcDir . ".vimrc.file"
" 検索の挙動に関する設定:
execute "source " . s:vimrcDir . ".vimrc.search"
" 編集(insertモード)に関する設定(補完、自動入力,カーソル行に対する設定,syntax
execute "source " . s:vimrcDir . ".vimrc.input"
" コマンドに関する設定(補完など)
execute "source " . s:vimrcDir . ".vimrc.command"
" GUI固有ではない画面表示の設定,非表示文字の設定
execute "source " . s:vimrcDir . ".vimrc.screen"
" titleバー及びstatusバーの設定
execute "source " . s:vimrcDir . ".vimrc.title_status"
" pluginの設定
if isdirectory(expand($DotVim . 'neoBundle/'))
    execute "source " . s:vimrcDir . ".vimrc.plugin"
endif

" os固有の設定
let s:osVimrc = expand(s:vimrcDir . ".vimrc." . g:os)
if filereadable(s:osVimrc)
    execute "source " . s:osVimrc
endif

" 環境固有設定
let s:localPref = expand(s:vimrcDir . ".vimrc.local")
if filereadable(s:localPref)
    execute "source " . s:localPref
endif
" ------------------------------------------------------------------------------------------
"           commands
" ------------------------------------------------------------------------------------------
" vimrc編集・反映コマンド定義
command! Ev edit   $MYVIMRC
command! Rv source $MYVIMRC

" 保存と同時にsource
augroup vimfileAutoSource
    autocmd!
    "autocmd FileType vim autocmd! BufWritePost <buffer> source %
    autocmd FileType vim command!-buffer W w | source %
augroup END

" ------------------------------------------------------------------------------------------
"           keymapping
" ------------------------------------------------------------------------------------------
" key-mapの待ち時間を設定
set timeoutlen=500

"help参照key-mapping
nnoremap <c-h>  :help<space>
nnoremap <c-h>g :helpgrep<space>
nnoremap <c-h>v :vertical help<space>
