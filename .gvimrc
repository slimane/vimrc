".gvimrcの場所により呼び出すfileの場所を設定"
let $DotGVim = expand(fnamemodify($MYVIMRC, ':h'))

scriptencoding utf-8

"メニューおよびツールバーを非表示化
set guioptions-=m
set guioptions-=T

"gvimrc editing/source commands
command! Eg :edit $MYGVIMRC
command! Rg :source $MYGVIMRC

"menuの文字化け回避"
source $VIMRUNTIME/delmenu.vim
set langmenu=ja_jp.utf-8
source $VIMRUNTIME/menu.vim

" local pref
let s:localPref = expand($DotGVim . '/.gvimrc.local')
if filereadable(s:localPref)
    execute 'source ' . s:localPref
endif

set background=dark
execute "colorscheme " . Case(g:os
\                           , {"win"    : "molokai"
\                           , "macunix" : "hemisu"})

let &guifont     = Case(g:os
\                   , {"win"    : iconv('Ricty_for_Powerline:h10:b:cSHIFTJIS', &encoding, 'cp932')
\                   , "macunix" : "Incosolate for Powerline:h10"
\                   , "default" : "Incosolate"})

let &guifontwide = Case(g:os
\                   , {"win"    : iconv('Osaka－等幅:h10:cSHIFTJIS', &encoding, 'cp932')
\                   , "macunix" : "Tanuki Permanent Marker for Powerline:h10"
\                   , "default" : "Incosolate"})

let &lines        = Case(g:os
\                   , {"win"    : "38"
\                   , "macunix" : "50"
\                   , "default" : "20"})

let &columns      = Case(g:os
\                   , {"win"    : "170"
\                   , "macunix" : "220"
\                   , "default" : "140"})

let &transparency = Case(g:os
\                   , {"win"    : "190"
\                   , "macunix" : "30"
\                   , "default" : "0"})

" windowsで透過が反映されないため。
if g:os ==# 'win'
    augroup sourceGvimrc
        autocmd! GUIEnter * source $MYGVIMRC
    augroup END
endif

" colorschemeの変更時にそのcolorschemeに対応した背景色を変更
command! -complete=color -nargs=? ColorScheme call BackgroupChange("<args>")
function! BackgroupChange(...) "{{{3
    let colorDefaultBg = {
    \                       "solarized"    : "light"
    \                       , "molokai"    : "dark"
    \                       , "hemisu"     : "dark"
    \                       , "railscasts" : "dark"
    \                       , "pyte"       : "light"
    \}
    if a:0 ==# 1
        if has_key(colorDefaultBg, a:1)
          let &background = colorDefaultBg[a:1]
        endif
        execute "colorscheme " . a:1
    else
        colorscheme
    endif
endfunction "}}}
